/** 
 * @file llwldaycycle_stub.cpp
 * @brief  stub class to allow unit testing
 *
 * $LicenseInfo:firstyear=2009&license=exoviewergpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2011, Linden Research, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

LLWLDayCycle::LLWLDayCycle(void)
{
}

LLWLDayCycle::~LLWLDayCycle(void)
{
}

bool LLWLDayCycle::getKeytime(LLWLParamKey keyFrame, F32& keyTime)
{
	keyTime = 0.5;
	return true;
}

bool LLWLDayCycle::removeKeyframe(F32 time)
{
	return true;
}

void LLWLDayCycle::loadDayCycleFromFile(const std::string& fileName)
{
}

void LLWLDayCycle::removeReferencesTo(const LLWLParamKey &keyframe)
{
}
