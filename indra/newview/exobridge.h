/** 
 * @file exobridge.h
 * @brief exoBridge class definitions
 *
 * $LicenseInfo:firstyear=2011&license=exoviewergpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_API
#define EXO_API

// Command prefixes:
#define API_CMD_PREFIX			'@' // For the new commands based on RLV's command structure.
#define GENESIS_CMD_PREFIX		'#' // For legacy Genesis commands to keep old Ark content working.

// Other non-included classes:
class LLViewerObject;

// Main class definition:
class eAPI
{
public:
	static BOOL phraseChat(std::string message, LLViewerObject* chatter, LLUUID from_id);
};

#endif // EXO_API
