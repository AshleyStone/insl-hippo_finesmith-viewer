/** 
 * @file exosystems.h
 * @brief exoSystems class definitions
 *
 * $LicenseInfo:firstyear=2011&license=exoviewergpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_SYSTEMS
#define EXO_SYSTEMS

#include "llsingleton.h"
#include "lleventtimer.h"

class exoSystems :
	public LLSingleton<exoSystems>,
	public LLEventTimer
{
public:
	exoSystems();

	static LLSD clientTags;
	static LLSD clientVersions;

	static std::string loginMessage;

	//static std::string updateTitle;
	//static std::string updateMessage;
	//static std::string updateURL;
	
	static std::string databaseBackendLocation;
	static std::string databaseBackendLabel;
	static std::string databaseBackendSalt;

	//static bool isBlacklisted;

	static bool hasLoginMessage;
	//static bool hasUpdate;
	static bool hasTags;
	static bool hasVersions;

	static bool disableSpacebar;

	static KEY mRTAKey;
	static KEY mRTDKey;

	static BOOL keyDown(KEY key, MASK mask, BOOL repeated);
	static BOOL keyUp(KEY key, MASK mask);

	static void getData();
	static void useLocalData();
	static void useReleaseData();
	static void phraseData(LLSD data);
	static void updateSettings();
	
	static std::string getClientTag(std::string id);
	static std::string getClientVersion(std::string id);

	static void handleTeleport(U32 type = 0);
	
	void smashDrawDistance();

	BOOL getStarted();

private:
	F32 drawDistance;
	
	virtual BOOL tick();
};

#endif // EXO_SYSTEMS
