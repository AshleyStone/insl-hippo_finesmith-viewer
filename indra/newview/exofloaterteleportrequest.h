/** 
 * @file exofloaterteleportrequest.h
 * @brief exoFloaterTeleportRequest class definition
 *
 * $LicenseInfo:firstyear=2011&license=exoviewergpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_TELEPORT_REQUEST
#define EXO_FLOATER_TELEPORT_REQUEST

#include "llfloater.h"
#include "llevents.h"

class LLButton;
class LLProgressBar;

class exoFloaterTeleportRequest :
	public LLFloater
{
	friend class LLFloaterReg;
	
private:
	exoFloaterTeleportRequest(const LLSD& key);

	BOOL postBuild();
	void draw();

public:
	LLTimer closeTimer;

	void setData(std::string from_name, LLUUID from_id, std::string message);
	void requestReply(std::string suffix = "");

	LLUUID targetKey;
	std::string targetMessage;
	std::string targetName;

private:
	BOOL previousCamera;

	void onAcceptButtonClicked();
	void onDeclineButtonClicked();
	void onMuteButtonClicked();

	void fixPosition();

	LLProgressBar* mProgressBar;
};

#endif // EXO_FLOATER_TELEPORT_REQUEST
