/** 
 * @file glowcombineF.glsl
 *
 * $LicenseInfo:firstyear=2007&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2007, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#ifdef DEFINE_GL_FRAGCOLOR
out vec4 gl_FragColor;
#endif

#extension GL_ARB_texture_rectangle : enable

uniform sampler2D glowMap;
uniform sampler2DRect screenMap;

VARYING vec2 vary_texcoord0;
VARYING vec2 vary_texcoord1;

//<exodus>
uniform vec3 exo_gamma;
uniform vec3 exo_exposure;
uniform vec3 exo_offset;
uniform float exo_vignette;

float Luminance(vec3 color)
{
	return dot(color, vec3(0.22, 0.707, 0.071));
}
//</exodus>

void main() 
{
	vec4 colcorr = texture2DRect(screenMap, vary_texcoord1.xy) + texture2D(glowMap, vary_texcoord0.xy);
	colcorr.rgb = max(vec3(0), colcorr.rgb + exo_offset);
	colcorr.rgb *= pow(vec3(2.0), exo_exposure);
	colcorr.rgb = pow(colcorr.rgb, exo_gamma);
	if (exo_vignette > 0)
	{
		vec2 tc = vary_texcoord0.xy - 0.5f;
		float vignette = 1 - dot(tc, tc);
		colcorr.rgb *= mix(1, vignette * vignette * vignette * vignette, exo_vignette);
	}
	
	gl_FragColor = colcorr;
}
