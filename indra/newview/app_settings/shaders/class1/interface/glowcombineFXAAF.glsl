/** 
 * @file glowcombineFXAAF.glsl
 *
 * $LicenseInfo:firstyear=2005&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2005, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */
 
#extension GL_ARB_texture_rectangle : enable

#ifdef DEFINE_GL_FRAGCOLOR
out vec4 gl_FragColor;
#endif

uniform sampler2DRect diffuseRect;

uniform vec2 screen_res;
VARYING vec2 vary_tc;

uniform float exo_gammafactor;
uniform float exo_texposure;
uniform float exo_vignette;

void main() 
{
	vec3 col = texture2DRect(diffuseRect, vary_tc*screen_res).rgb;
	
	if (exo_texposure > -1)
	{
		col.rgb *= exo_texposure;  // Hardcoded Exposure Adjustment
   		vec3 x = max(vec3(0), col.rgb-0.004);
   		vec3 retColor = (x*(6.2*x+.5))/(x*(6.2*x+1.7)+0.06);
		if (exo_vignette > 0)
		{
			vec2 tc = vary_tc - 0.5f;
			float vignette = 1 - dot(tc, tc);
			retColor *= mix(1, vignette * vignette * vignette * vignette, exo_vignette);
		}
		gl_FragColor = vec4(retColor, dot(retColor, vec3(0.299, 0.587, 0.144)));
	} else {
		col.rgb = pow(col.rgb, vec3(1.0/exo_gammafactor));
		if (exo_vignette > 0)
		{
			vec2 tc = vary_tc - 0.5f;
			float vignette = 1 - dot(tc, tc);
			col.rgb *= mix(1, vignette * vignette * vignette * vignette, exo_vignette);
		}
		gl_FragColor = vec4(col.rgb, dot(col.rgb, vec3(0.299, 0.587, 0.144)));
	}
}
