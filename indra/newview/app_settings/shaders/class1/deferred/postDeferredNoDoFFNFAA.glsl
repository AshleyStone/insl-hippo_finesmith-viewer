/** 
 * @file postDeferredF.glsl
 *
 * $LicenseInfo:firstyear=2007&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2007, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */
 


#extension GL_ARB_texture_rectangle : enable
#extension GL_EXT_gpu_shader4 : enable

uniform sampler2DRect diffuseRect;
uniform sampler2D bloomMap;
uniform sampler2DRect depthMap;

uniform vec2 screen_res;
varying vec2 vary_fragcoord;
uniform vec3 exposure;
uniform vec3 offset;
uniform vec3 gamma;
uniform mat4 inv_proj;


float Luminance( vec3 c )
{
	return dot( c, vec3(0.2125,0.7154,0.072) );
}

//NFAA START

vec4 NFAASHADER(sampler2DRect tex, vec2 uv, float depth)
{
	float NFAA_SENSITIVITY = -1024;
	vec2 vPixelViewport = normalize(vec2(1 / screen_res.x, 1 / screen_res.y / 2)) * 1.5;
	vec2 upOffset =normalize(vec2( 0.0f,vPixelViewport.y)) * 0.064;
	vec2 rightOffset =normalize(vec2(vPixelViewport.x, 0.0f )) * 0.064;
	float topHeight = Luminance( texture2DRect( tex, uv+upOffset).xyz );
	float bottomHeight = Luminance( texture2DRect( tex, uv-upOffset).xyz );
	float rightHeight = Luminance( texture2DRect( tex, uv+rightOffset).xyz );
	float leftHeight = Luminance( texture2DRect( tex, uv-rightOffset).xyz );
	float leftTopHeight =Luminance( texture2DRect( tex, uv-rightOffset+upOffset).xyz );
	float leftBottomHeight = Luminance( texture2DRect( tex, uv-rightOffset-upOffset).xyz );
	float rightBottomHeight = Luminance( texture2DRect( tex, uv+rightOffset+upOffset).xyz );
	float rightTopHeight = Luminance( texture2DRect(tex, uv+rightOffset-upOffset).xyz );
	float sum0 = rightTopHeight+ bottomHeight + leftTopHeight;
	float sum1 = leftBottomHeight + topHeight + rightBottomHeight;
	float sum2 = leftTopHeight + rightHeight + leftBottomHeight;
	float sum3 = rightBottomHeight + leftHeight + rightTopHeight;
	float vecs1 = (sum0 - sum1) * NFAA_SENSITIVITY;
	float vecs2 = (sum3 - sum2) * NFAA_SENSITIVITY;
	vec2 n = normalize(vec2( vecs1, vecs2) *vPixelViewport*(2-depth));
	vec4 s0 = texture2DRect( tex, uv );
	vec4 s1 = texture2DRect(  tex, uv + n.xy );
	vec4 s2 = texture2DRect(  tex, uv - n.xy );
	vec4 s3 = texture2DRect(  tex, uv + vec2(n.x, -n.y) );
	vec4 s4 = texture2DRect(  tex, uv - vec2(n.x, -n.y) );
	return (s0 + s1 + s2 + s3 + s4) * 0.2;
}

//NFAA END

float getDepth(vec2 pos_screen)
{
	float z = texture2DRect(depthMap, pos_screen.xy).r;
	z = z*2.0-1.0;
	vec4 ndc = vec4(0.0, 0.0, z, 1.0);
	vec4 p = inv_proj*ndc;
	return p.z/p.w;
}

void main() 
{
	float depth = getDepth(vary_fragcoord.xy);
	vec3 transexp = pow(vec3(2.0), exposure);
	vec4 diff = NFAASHADER(diffuseRect, vary_fragcoord.xy, depth);
	diff.rgb = max(vec3(0), diff.rgb) + offset;
	diff.rgb *= transexp;
	diff.rgb = pow(diff.rgb, gamma);
	
	vec4 bloom = texture2D(bloomMap, vary_fragcoord.xy/screen_res);
	gl_FragColor = diff.xyzz + bloom;
}
