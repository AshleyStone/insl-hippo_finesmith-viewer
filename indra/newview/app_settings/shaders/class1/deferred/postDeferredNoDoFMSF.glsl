/** 
 * @file postDeferredF.glsl
 *
 * $LicenseInfo:firstyear=2007&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2007, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */
 


#extension GL_ARB_texture_rectangle : enable
#extension GL_ARB_texture_multisample : enable

uniform sampler2DMS diffuseRect;
uniform sampler2D bloomMap;

uniform vec2 screen_res;
varying vec2 vary_fragcoord;

vec4 texture2DMS(sampler2DMS tex, ivec2 tc)
{
	vec4 ret = vec4(0,0,0,0);

	for (int i = 0; i < samples; ++i)
	{
		 ret += texelFetch(tex,tc,i);
	}

	return ret/samples;
}

void main() 
{
	vec4 diff = texture2DMS(diffuseRect, ivec2(vary_fragcoord.xy));
	vec3 transexp = pow(vec3(2.0), exo_exposure);
	
	vec4 bloom = texture2D(bloomMap, vary_fragcoord.xy/screen_res);
	vec4 colcorr = bloom + diff;
	colcorr.rgb = max(vec3(0), colcorr.rgb + exo_offset);
	colcorr.rgb *= transexp;
	colcorr.rgb = pow(colcorr.rgb, exo_gamma);
	gl_FragColor = colcorr;
}
