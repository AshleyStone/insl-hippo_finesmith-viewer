/** 
 *
 * Copyright (c) 2009-2011, Kitty Barnett
 * 
 * The source code in this file is provided to you under the terms of the 
 * GNU Lesser General Public License, version 2.1, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. Terms of the LGPL can be found in doc/LGPL-licence.txt 
 * in this distribution, or online at http://www.gnu.org/licenses/lgpl-2.1.txt
 * 
 * By copying, modifying or distributing this software, you acknowledge that
 * you have read and understood your obligations described above, and agree to 
 * abide by those obligations.
 * 
 */

#ifndef RLV_EXTENSIONS_H
#define RLV_EXTENSIONS_H

#include "rlvcommon.h"

// ============================================================================
/*
 * RlvExtGetSet
 * ============
 * Implements @get_XXX:<option>=<channel> and @set_XXX:<option>=force
 *
 */

class RlvExtGetSet : public RlvCommandHandler
{
public:
	RlvExtGetSet();
	virtual ~RlvExtGetSet() {}

	virtual bool onForceCommand(const RlvCommand& rlvCmd, ERlvCmdRet& cmdRet);
	virtual bool onReplyCommand(const RlvCommand& rlvCmd, ERlvCmdRet& cmdRet);
protected:
	std::string onGetDebug(std::string strSetting);
	std::string onGetPseudoDebug(const std::string& strSetting);
	ERlvCmdRet  onSetDebug(std::string strSetting, const std::string& strValue);
	ERlvCmdRet  onSetPseudoDebug(const std::string& strSetting, const std::string& strValue);

	bool processCommand(const RlvCommand& rlvCmd, ERlvCmdRet& eRet);

public:
	enum { DBG_READ = 0x01, DBG_WRITE = 0x02, DBG_PERSIST = 0x04, DBG_PSEUDO = 0x08 };
	static std::map<std::string, S16> m_DbgAllowed;
	static std::map<std::string, std::string> m_PseudoDebug;

	static bool findDebugSetting(/*[in,out]*/ std::string& strSetting, /*[out]*/ S16& flags);
	static S16  getDebugSettingFlags(const std::string& strSetting);
};

// ============================================================================

struct WLColorControl;
struct WLFloatControl;

class RlvWindLightControl
{
public:
	enum EType			 { TYPE_COLOR, TYPE_COLOR_R, TYPE_FLOAT, TYPE_UNKNOWN };
	enum EColorComponent { COMPONENT_R, COMPONENT_G, COMPONENT_B, COMPONENT_I, COMPONENT_NONE };
public:
	RlvWindLightControl(WLColorControl* pCtrl, bool fColorR) : m_eType((!fColorR) ? TYPE_COLOR: TYPE_COLOR_R), m_pColourCtrl(pCtrl), m_pFloatCtrl(NULL) {}
	RlvWindLightControl(WLFloatControl* pCtrl) : m_eType(TYPE_FLOAT), m_pColourCtrl(NULL), m_pFloatCtrl(pCtrl) {}

	EType		getControlType() const	{ return m_eType; }
	bool		isColorType() const		{ return (TYPE_COLOR == m_eType) || (TYPE_COLOR_R == m_eType); }
	bool		isFloatType() const		{ return (TYPE_FLOAT == m_eType); }
	// TYPE_COLOR and TYPE_COLOR_R
	F32			getColorComponent(EColorComponent eComponent, bool& fError) const;
	LLVector4	getColorVector(bool& fError) const;
	bool		setColorComponent(EColorComponent eComponent, F32 nValue);
	// TYPE_FLOAT
	F32			getFloat(bool& fError) const;
	bool		setFloat(F32 nValue);

	static EColorComponent getComponentFromCharacter(char ch);
protected:
	EType			m_eType;			// Type of the WindLight control
	WLColorControl*	m_pColourCtrl;
	WLFloatControl*	m_pFloatCtrl;
};

// ============================================================================

class RlvWindLight : public LLSingleton<RlvWindLight>
{
	friend class LLSingleton<RlvWindLight>;
public:
	RlvWindLight();

	std::string	getValue(const std::string& strSetting, bool& fError);
	bool		setValue(const std::string& strRlvName, const std::string& strValue, const bool force = true);

protected:
	std::map<std::string, RlvWindLightControl> m_ControlLookupMap;
};

// ============================================================================

#endif // RLV_EXTENSIONS_H
