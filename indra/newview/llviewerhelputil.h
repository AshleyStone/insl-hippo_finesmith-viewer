/** 
 * @file llviewerhelputil.h
 * @brief Utility functions for the Help system
 * @author Soft Linden
 *
 * $LicenseInfo:firstyear=2009&license=exoviewergpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#ifndef LL_LLVIEWERHELPUTIL_H
#define LL_LLVIEWERHELPUTIL_H

class LLControlGroup;
class LLOSInfo;

class LLViewerHelpUtil
{
 public:
	static std::string helpURLEncode( const std::string &component );
	static std::string buildHelpURL( const std::string &topic );
};

#endif // header guard
