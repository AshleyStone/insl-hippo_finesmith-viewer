/** 
 * @file exosystems.h
 * @brief evonManager class definitions
 *
 * $LicenseInfo:firstyear=2011&license=exoviewergpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EVON_MANAGER
#define EVON_MANAGER

class evonManager
{
private:
	evonManager(){}

public:
	static S32 toEVON(const LLSD& sd, std::ostream& str, std::string type = "UNDEF", std::string version = "0.0");
	static S32 toPrettyEVON(const LLSD& sd, std::ostream& str, std::string type = "UNDEF", std::string version = "0.0");
	static S32 fromEVON(LLSD& sd, std::istream& str, std::string type = "UNDEF", double version = 0.0);
};

#endif // EVON_MANAGER
