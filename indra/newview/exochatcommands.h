/** 
 * @file exochatcommands.h
 * @brief exoChatCommands class definitions
 *
 * $LicenseInfo:firstyear=2011&license=exoviewergpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_CHAT_COMMANDS
#define EXO_CHAT_COMMANDS

class exoChatCommands
{
private:
	exoChatCommands(){}

public:
	static bool phraseChat(std::string data);

private:
	static void systemTip(std::string data);
	static void systemMessage(std::string data);
};

#endif // EXO_CHAT_COMMANDS
