/** 
 * @file exofloaterstatistics.h
 * @brief EXOFloaterStatistics class definition
 *
 * $LicenseInfo:firstyear=2011&license=exoviewergpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_TEXT_RADAR
#define EXO_FLOATER_TEXT_RADAR

#include "llfloater.h"
#include "lleventtimer.h"

class LLTextBox;

class exoFloaterTextRadar :
	public LLFloater,
	public LLEventTimer
{
	friend class LLFloaterReg;

private:
	exoFloaterTextRadar(const LLSD& key);

	BOOL postBuild();

	void draw();
	virtual BOOL tick();

	LLTextBox* mText;

	BOOL onDoubleClick(LLUICtrl* ctrl, S32 x, S32 y, MASK mask);

	void textAlignLeft();
	void textAlignCenter();
	void textAlignRight();

	LLRect windowSize;

	bool renderBackground;
};

#endif // EXO_FLOATER_TEXT_RADAR
