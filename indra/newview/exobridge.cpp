/** 
 * @file exobridge.cpp
 *
 * eAPI is a simple set of tools designed to provide content
 * creators with the opportunity to use optional viewer side functions
 * in Exodus for their products in Second Life.
 *
 * $LicenseInfo:firstyear=2011&license=exoviewergpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * version 2.0 of the License only.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exobridge.h"

// Stuff
#include "llagent.h"
#include "llstartup.h"
#include "llviewercontrol.h"
#include "llviewermessage.h"
#include "llviewerobject.h"
#include "llviewerobjectlist.h"
#include "llviewerwindow.h"

// Raycasting
#include "pipeline.h"
#include "raytrace.h"

// Windlight
#include "rlvextensions.h"
#include "llwlparammanager.h"
#include "llenvmanager.h"
#include "lldaycyclemanager.h"

// Camera
#include "llfocusmgr.h"
#include "llvoavatarself.h"
#include "llagentcamera.h"

// Chat given from scripts is handled here:
BOOL eAPI::phraseChat(std::string message, LLViewerObject* chatter, LLUUID from_id)
{
	if (message.length() < 4) return FALSE;

	if (GENESIS_CMD_PREFIX == message[0])
	{
		/**
		 * PLEASE NOTE:
		 *
		 * This is some of the Genesis script bridge code added for Ark's MediOrb support, 
		 * The code below will soon to be completely replaced with "eAPI" which will be more
		 * simular to RLV, so please do not make content that relies on these commands below,
		 * because they will be changed.
		 */

		// Todo: Merge commands into eAPI processers where possible.

		if(message.substr(0, 5) == "#sit:" && LLUUID::validate(message.substr(5)))
		{
			if(!gSavedSettings.getBOOL("ExodusGenesisAPI")) return TRUE;

			LLUUID idTarget(message.substr(5));
			LLMessageSystem	*msg = gMessageSystem;

			msg->newMessageFast(_PREHASH_AgentRequestSit);
			msg->nextBlockFast(_PREHASH_AgentData);
			msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
			msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
			msg->nextBlockFast(_PREHASH_TargetObject);
			msg->addUUIDFast(_PREHASH_TargetID, idTarget);
			msg->addVector3Fast(_PREHASH_Offset, LLVector3::zero);

			gAgent.sendReliableMessage();

			return TRUE;
		}
		else if(message.substr(0, 5) == "#rez:")
		{
			if(!gSavedSettings.getBOOL("ExodusGenesisAPI")) return TRUE;

			LLSD args;
			U32 count = 0;
			std::string token;
			std::istringstream iss(message.substr(5));
			while(getline(iss, token, ',')){ args[count] = token; count += 1; }

			std::string inv_name = args[0].asString();

			if(!inv_name.empty() && args.size() == 4)
			{
				F32 x, z;
				F32 y = args[2].asReal();
				std::string temp;

				if((temp = args[1].asString()) != "" && temp[0] == '<') x = atof(temp.substr(1).c_str());
				else x = args[1].asReal();

				if((temp = args[3].asString()) != "" && temp[temp.length() - 1] == '>') z = atof(temp.substr(0, temp.length() - 1).c_str());
				else z = args[3].asReal();

				LLViewerObject* from_object = gObjectList.findObject(from_id);
				if(from_object)
				{
					LLInventoryObject::object_list_t obj_inv;
					from_object->getInventoryContents(obj_inv);
					if(!obj_inv.empty())
					{
						LLVector3 target = LLVector3(llclamp(x, 0.f, 256.f), llclamp(y, 0.f, 256.f), llclamp(z, 0.f, 4096.f));
						LLInventoryObject::object_list_t::iterator obj_it;
						for(obj_it = obj_inv.begin(); obj_it != obj_inv.end(); ++obj_it)
						{
							LLInventoryItem* inventory = (LLInventoryItem*)((LLInventoryObject*)(*obj_it));

							if(inventory->getName() == inv_name)
							{
								LLPermissions perms = inventory->getPermissions();

								U32 group_mask		= perms.getMaskGroup();
								U32 everyone_mask	= perms.getMaskEveryone();
								U32 next_owner_mask	= perms.getMaskNextOwner();

								LLMessageSystem	*msg = gMessageSystem;

								msg->newMessageFast(_PREHASH_RezObject);
								msg->nextBlockFast(_PREHASH_AgentData);
								msg->addUUIDFast(_PREHASH_AgentID,  gAgent.getID());
								msg->addUUIDFast(_PREHASH_SessionID,  gAgent.getSessionID());
								msg->addUUIDFast(_PREHASH_GroupID, gAgent.getGroupID());
								msg->nextBlock("RezData");
								msg->addUUIDFast(_PREHASH_FromTaskID, from_id);
								msg->addU8Fast(_PREHASH_BypassRaycast, (U8)TRUE);
								msg->addVector3Fast(_PREHASH_RayStart, target);
								msg->addVector3Fast(_PREHASH_RayEnd, target);
								msg->addUUIDFast(_PREHASH_RayTargetID, LLUUID::null);
								msg->addBOOLFast(_PREHASH_RayEndIsIntersection, FALSE);
								msg->addBOOLFast(_PREHASH_RezSelected, FALSE);
								msg->addBOOLFast(_PREHASH_RemoveItem, FALSE);
								msg->addU32Fast(_PREHASH_ItemFlags, inventory->getFlags());
								msg->addU32Fast(_PREHASH_GroupMask, group_mask);
								msg->addU32Fast(_PREHASH_EveryoneMask, everyone_mask);
								msg->addU32Fast(_PREHASH_NextOwnerMask, next_owner_mask);
								msg->nextBlockFast(_PREHASH_InventoryData);
								inventory->packMessage(msg);

								gAgent.sendReliableMessage();
							}
						}
					}
				}
			}

			return TRUE;
		}
		else if(message == "#load")
		{
			if(gSavedSettings.getBOOL("ExodusGenesisAPI"))
			{
				if(LLStartUp::getStartupState() >= STATE_STARTED && chatter) chatter->requestInventory();
			}

			return TRUE;
		}
		else if(message.substr(0, 4) == "#rc:") // OH DEAR GOD WHY.
		{
			if(!gSavedSettings.getBOOL("ExodusGenesisAPI")) return TRUE;

			LLSD args;
			U32 count = 0;

			std::string token;
			std::istringstream iss(message.substr(4));

			while(getline(iss, token, ',')){ args[count] = token; ++count; }

			if(args[0].asString() == "once" && args.size() > 1)
			{
				std::string label = (args.size() > 3) ? args[3].asString() : "once";
				S32 x = gViewerWindow->getCurrentMouseX();
				S32 y = gViewerWindow->getCurrentMouseY();
				F32 depth = ((args.size() == 3) ? args[2].asReal() : 256.f);
				LLVector3 mouse_direction_global = gViewerWindow->mouseDirectionGlobal(x, y);
				LLVector3 mouse_point_global = LLViewerCamera::getInstance()->getOrigin();
				LLVector3 n = LLViewerCamera::getInstance()->getAtAxis();
				LLVector3 p = mouse_point_global + n * LLViewerCamera::getInstance()->getNear();
				LLVector3 target; line_plane(mouse_point_global, mouse_direction_global, p, n, target);
				LLVector3 source = target + mouse_direction_global * depth;
				S32 castFace = -1;
				LLVector3 castIntersection = LLVector3(0.f);
				LLVector2 castTextureCoord;
				LLVector3 castNormal;
				LLVector3 castBinormal;

				if(from_id.isNull()) from_id = gAgent.getID();

				LLViewerObject* castObject = NULL;

				castObject = gPipeline.lineSegmentIntersectInWorld(target, source, FALSE, &castFace, &castIntersection, &castTextureCoord, &castNormal, &castBinormal);

				LLVector3 translate = castIntersection;
				LLMessageSystem	*msg = gMessageSystem;

				msg->newMessage("ScriptDialogReply");
				msg->nextBlock("AgentData");
				msg->addUUID("AgentID", gAgent.getID());
				msg->addUUID("SessionID", gAgent.getSessionID());
				msg->nextBlock("Data");
				msg->addUUID("ObjectID", from_id);
				msg->addS32("ChatChannel", args[1].asInteger());
				msg->addS32("ButtonIndex", 1);

				if(castObject)
				{
					LLCoordFrame orient;
					orient.lookDir(castNormal, castBinormal);
					LLQuaternion rotation = orient.getQuaternion();
					std::string key = castObject->getID().asString();

					if(from_id.isNull()) castObject->getID();

					msg->addString("ButtonLabel", llformat(
						"#rc,<%f,%f,%f>,<%f,%f,%f,%f>,",
						translate.mV[0], translate.mV[1],translate.mV[2],
						rotation.mQ[0], rotation.mQ[1], rotation.mQ[2], rotation.mQ[3]) +
						key + "," + label + llformat(",%d", gFrameCount)
					);
				}
				else
				{
					if(!translate.isNull())
					{
						LLCoordFrame orient;
						orient.lookDir(castNormal, castBinormal);
						LLQuaternion rotation = orient.getQuaternion();

						msg->addString("ButtonLabel", llformat(
							"#rc,<%f,%f,%f>,<%f,%f,%f,%f>,",
							translate.mV[0], translate.mV[1],translate.mV[2],
							rotation.mQ[0], rotation.mQ[1], rotation.mQ[2], rotation.mQ[3]) +
							"null," + label + llformat(",%d", gFrameCount)
						);
					}
					else msg->addString("ButtonLabel", llformat("#rc,null,null,null,%s,%d", label.c_str(), gFrameCount));
				}

				gAgent.sendReliableMessage();
			}

			return TRUE;
		}
		else if(message.substr(0, 4) == "#wl:")
		{
			if(!gSavedSettings.getBOOL("ExodusGenesisAPI")) return TRUE;

			if(message == "#wl:save")
			{
				// Apparently this isn't needed anymore?
				//LLEnvManagerNew::instance().saveUserPrefs();
			}
			else if(message == "#wl:load")
			{
				LLEnvManagerNew::instance().usePrefs();
			}
			else if(message == "#wl:clear")
			{
				LLEnvManagerNew::instance().usePrefs();
			}
			else
			{
				LLSD args;
				U32 count = 0;
				std::string token;
				std::istringstream iss(message.substr(4));
				
				while(getline(iss, token, '=')){ args[count] = token; ++count; }

				if(count > 1)
				{
					RlvWindLight::instance().setValue(args[0], args[1], true);
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}
